﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace iSpyApplication
{
    public delegate void Notify(string IP);  // delegate

    internal class ExecuteServer
    {
        public event Notify NewMotionEvent; // event

        private TcpListener _Listener;
        private List<StreamWriter> _Clients;


        public ExecuteServer()
        {
            _Clients = new List<StreamWriter>();
            NewMotionEvent += ExecuteServer_NewMotionEvent;
        }

        public void NewMotion(string IP)
        {
            NewMotionEvent?.Invoke(IP);
        }

        private void ExecuteServer_NewMotionEvent(string IP)
        {
            while (_Clients.Any())
                try
                {
                    _Clients[0].WriteLine(IP);
                    _Clients[0].Flush();
                    return;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error sending to client, " + ex.Message);
                    _Clients.RemoveAt(0);
                }
        }

        /// <summary>
        /// Start the listener server
        /// </summary>
        internal void StartListener(int port)
        {
            if (_Listener != null)
                return;

            try
            {
                _Listener = new TcpListener(IPAddress.Any, port);
                _Listener.Start();

                while (true)
                    Accept(_Listener.AcceptTcpClient());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
            finally { _Listener.Stop(); }
        }

        /// <summary>
        /// Stop the listener server
        /// </summary>
        internal void StopListener()
        {
            _Listener?.Stop();
            _Listener = null;
        }

        /// <summary>
        /// Handel each new client request
        /// </summary>
        /// <param name="client"></param>
        private void Accept(TcpClient client)
        {
            _Clients.Add(new StreamWriter(client.GetStream()));
        }
    }
}
